import numpy as np
import pandas as pd
import plotly as pl
import plotly.graph_objs as go
from helpers import *
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures

#Reading data
df = pd.read_csv('espressoYourself.txt')

#Select points by inserting into brands
brands = ['kjeldsberg', 'farmers','evergood']
selected = df.loc[df['Brand'].isin(brands)]


#Adding points, each brand with a different color
data = []
color_list = colors(len(brands))
i = 0
for brand in brands:
    selected = df.loc[df['Brand'] == brand]
    points = selected.iloc[:,1:].values
    scatter_trace = go.Scatter3d(
                    x = points[:,0],
                    y = points[:,1],
                    z = points[:,2],
                    mode = 'markers',
                    marker = dict(
                        color = color_list[i]
                    ),
                    name = brand
                )
    data.append(scatter_trace)
    i += 1


#Doing linear regression on all points to generate surface
selected = df.loc[df['Brand'].isin(brands)]
points = selected.iloc[:,1:].values

X = points[:,(0,1)] #Features
z = points[:,2] #Targets

#Transforming to higher dimensional linear data
poly_features = PolynomialFeatures(degree = 3)
X_lin = poly_features.fit_transform(X)

#Linear regression
model = LinearRegression()
model.fit(X_lin, z)
z_fit = model.predict(X_lin)

#Adding surface for input points
#Should be exchanged with a smoother surface on a reasonable grid
surface_trace = go.Mesh3d(
                    x = x,
                    y = y,
                    z = z_fit,
                    opacity = 0.5,
                    color = 'green'
                )
data.append(surface_trace)

#Adding layout to plot and plotting in same directory
layout = dict(
    scene = dict(
        xaxis = dict(title = 'Water'),
        yaxis = dict(title = 'Coffee'),
        zaxis = dict(title = 'Goodness')
        )
)
fig = go.Figure(data = data, layout = layout)
pl.offline.plot(fig, filename = 'coffeeSurface.html')




















######################################Extra#####################################
#Beginning of meshplot for smooth surface
# x = points[:,0]
# y = points[:,1]
# x_grid = np.linspace(x.min()-1, x.max()+1, num = 50)
# y_grid = np.linspace(y.min()-1, y.max()+1, num = 50)
# x_mesh, y_mesh = np.meshgrid(x_grid,y_grid)
# x_mesh.shape
# y_mesh.shape
#
#
# X_grid = np.array([x_grid, y_grid]).transpose()
# X_grid_lin = poly_features.fit_transform(X_grid)
# z_fit = model.predict(X_grid_lin)
# x_grid.shape
# y_grid.shape
# z_fit.shape
#
# x.shape
# y.shape
# z.shape
# X.shape
# X_lin.shape
# model.predict(X_lin).shape
#
# x_grid.shape
# y_grid.shape
# X_grid.shape
# X_grid_lin.shape
# z_fit.shape
#
#
#
# surface_trace = go.Mesh3d(
#                     x = x_grid,
#                     y = y_grid,
#                     z = z_fit,
#                     opacity = 0.5,
#                     color = 'blue'
#                 )
# data.append(surface_trace)
#
