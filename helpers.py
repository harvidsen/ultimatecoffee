import random


def colors(n):
    '''Borrowed from Vivek Nagarajan, from Quora, with a slight modification'''
    ret = []
    r = int(random.random() * 256)
    g = int(random.random() * 256)
    b = int(random.random() * 256)
    step = 256 / n

    for i in range(n):
        r += step
        g += step
        b += step
        r = int(r) % 256
        g = int(g) % 256
        b = int(b) % 256
        c_string = 'rgb({},{},{})'.format(r,g,b)
        ret.append(c_string)

    return ret
